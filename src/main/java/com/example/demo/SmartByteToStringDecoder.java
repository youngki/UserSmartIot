package com.example.demo;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.util.ReferenceCountUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;

import java.util.List;

/**
 * 智能字节字符串解码器
 *
 * @author youngki
 * @date 2022/10/20
 */
@Slf4j
public class SmartByteToStringDecoder extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        int readableBytes = byteBuf.readableBytes();
        //假设需要解析 int 类型的消息（int 4个字节）
        if (readableBytes > 0) {
            byte[] bytes = new byte[byteBuf.readableBytes()];
            byteBuf.readBytes(bytes);//读字节
            String data = Hex.encodeHexString(bytes);
            ReferenceCountUtil.retain(data);
            list.add(data);
        }
    }
}
