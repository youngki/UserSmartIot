package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 演示应用程序
 *
 * @author youngki
 * @date 2022/10/20
 */
@EnableScheduling
@EnableAsync
@SpringBootApplication
public class DemoApplication  implements CommandLineRunner {

    @Autowired
    NettyServer nettyServer;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        Thread kean = new Thread(new KeanUser());
        kean.start();
    }


    /**
     * 用户信息传输装置
     *
     * @author youngki
     * @date 2022/10/20
     */
    private class KeanUser implements Runnable{

        @Override
        public void run() {
            try {
                nettyServer.userDataByKeAn(7799);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
